#ifndef FORMA_GEOMETRICA_HPP
#define FORMA_GEOMETRICA_HPP
#include <iostream>

using namespace std;

class FormaGeometrica{
	protected:
		float base;
		float altura;
		float area;
		float perimetro;
		int lados;

	public:
		FormaGeometrica(int lados, float base, float altura);
		FormaGeometrica();
		float getBase();
		void setBase(float base);
		float getAltura();
		void setAltura(float altura);
		int getLados();
		void setLados(int lados);
		float getArea();
		float getPerimetro();
		void calculaArea(float base, float altura);
		float calculaArea();
		void calculaPerimetro(float base, float altura);
		float calculaPerimetro();
};
#endif
