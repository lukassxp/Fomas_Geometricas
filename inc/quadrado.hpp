#ifndef QUADRADO_HPP
#define QUADRADO_HPP

#include "formaGeometrica.hpp"

class Quadrado : public FormaGeometrica{
	private:
		float lado;
	public:
		Quadrado();
		void setLado(float lado);
		float getLado();
};
#endif
