#include "formaGeometrica.hpp"

FormaGeometrica::FormaGeometrica(int lados, float base, float altura){
	this->lados = lados;
	this->base = base;
	this->altura = altura;
}

FormaGeometrica::FormaGeometrica(){

}

int FormaGeometrica::getLados(){
	return lados;
}

void FormaGeometrica::setLados(int lados){
	this->lados = lados;
}

float FormaGeometrica::getBase(){
	return base;
}

void FormaGeometrica::setBase(float base){
	this->base = base;
}

float FormaGeometrica::getAltura(){
	return altura;
}

void FormaGeometrica::setAltura(float altura){
	this->altura = altura;
}

float FormaGeometrica::getArea(){
	return area;
}

float FormaGeometrica::getPerimetro(){
	return perimetro;
}

void FormaGeometrica:: calculaArea(float base, float altura){
	area = base * altura;
}
		

float FormaGeometrica::calculaArea(){
	area = base * altura;	
	return area;
}
		


void FormaGeometrica::calculaPerimetro(float base, float altura){
	perimetro = (base * 2) + (altura * 2);
}

float FormaGeometrica::calculaPerimetro(){
	perimetro = (base * 2) + (altura * 2);
	return perimetro;
}
