#include <iostream>
#include "formaGeometrica.hpp"
#include "circulo.hpp"
#include "retangulo.hpp"
#include "triangulo.hpp"
#include "quadrado.hpp"

using namespace std;

int main(void){
	Circulo* c = new Circulo(); 
	Quadrado* q = new Quadrado();
	Retangulo* r = new Retangulo();
	Triangulo* t = new Triangulo();

	c->setRaio(5);
	c->calculaArea();
	c->calculaPerimetro();

	cout << "*---------------------------------------------------------------*\n" << endl;
	cout << "FIGURA: CIRCULO\n\nRAIO = " << c->getRaio() << "\nAREA = " << c->getArea() << endl;
	cout << "PERIMETRO = " << c->getPerimetro() << "\nLADOS = " << c->getLados() << "\n" << endl;

	q->setLado(10);
	q->calculaArea();
	q->calculaPerimetro();

	cout << "*---------------------------------------------------------------*\n" << endl;
	cout << "FIGURA: QUADRADO\n\nLADO = " << q->getLado() << "\nAREA = " << q->getArea() << endl;
	cout << "PERIMETRO = " << q->getPerimetro() << "\nLADOS = " << q->getLados() << "\n" << endl;

	r->setBase(20);
	r->setAltura(15);
	r->calculaArea();
	r->calculaPerimetro();

	cout << "*---------------------------------------------------------------*\n" << endl;
	cout << "FIGURA: RETANGULO\n\nBASE = " << r->getBase() << "\nALTURA = " << r->getAltura() << "\nAREA = " << r->getArea() << endl;
	cout << "PERIMETRO = " << r->getPerimetro() << "\nLADOS = " << r->getLados() << "\n" << endl;

	t->setBase(10);
	t->setAltura(8.66);
	t->calculaArea();
	t->calculaPerimetro();

	cout << "*---------------------------------------------------------------*\n" << endl;
	cout << "FIGURA: TRIANGULO\n\nBASE = " << t->getBase() << "\nALTURA = " << t->getAltura() << "\nAREA = " << t->getArea() << endl;
	cout << "PERIMETRO = " << t->getPerimetro() << "\nLADOS = " << t->getLados() << "\n" << endl;

	FormaGeometrica * formas[5] = {c, q, r, t, new FormaGeometrica(4, 5.5, 8.7)};

	formas[4]->calculaArea(formas[4]->getBase(), formas[4]->getAltura());
	formas[4]->calculaPerimetro(formas[4]->getBase(), formas[4]->getAltura());

	cout << "*---------------------------------------------------------------*\n" << endl;
	cout << "FIGURA: DESCONHECIDA\n\nBASE = " << formas[4]->getBase() << "\nALTURA = " << formas[4]->getAltura() << "\nAREA = " << formas[4]->getArea();
	cout << "\nPERIMETRO = " << formas[4]->getPerimetro() << "\nLADOS = " << formas[4]->getLados() << "\n" << endl;
	cout << "*---------------------------------------------------------------*\n" << endl;
	
 	for (int i = 0; i < 5; i++) {
		formas[i]->calculaArea();
		formas[i]->calculaPerimetro();
		cout << "A forma geometrica de " << formas[i]->getLados();
		cout << " lados tem área = " << formas[i]->getArea();
		cout << " e perímetro = " << formas[i]->getPerimetro();
		cout << endl;
	}

	cout << "\n*---------------------------------------------------------------*\n" << endl;

	return 0;
}
