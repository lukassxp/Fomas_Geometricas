#include "circulo.hpp"

Circulo::Circulo(){
	lados = 0;
}

void Circulo::setRaio(float raio){
	this->raio = raio;
}

float Circulo::getRaio(){
	return raio;
}

float Circulo::calculaArea(){
	area = (3.14 * (raio * raio));
	return area;
}

float Circulo::calculaPerimetro(){
	perimetro = (2 * 3.14 * raio);	
	return perimetro;
}
